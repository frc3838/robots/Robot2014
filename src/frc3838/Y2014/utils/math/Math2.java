package frc3838.Y2014.utils.math;


import com.sun.squawk.util.MathUtils;



public final class Math2
{
    private Math2() {}


    /**
     * Converts a decimal to a percentage String (including the '%' sign) with no decimal places and no rounding.
     *
     * @param value the value to convert to a percentage
     *
     * @return the value as a percentage String
     */
    public static String toPercentage(double value)
    {
        return toPercentage(value, 0, false);
    }


    /**
     * Converts a decimal number to a percentage String (including the '%' sign).
     *
     * @param value     the value to convert to a percentage
     * @param precision the number of decimal places to have in the percentage value; must be between 0 and 5 inclusive
     * @param round     if true, the value will be rounded up, otherwise irt will be floored
     *
     * @return the value as a percentage String
     */
    public static String toPercentage(double value, int precision, boolean round)
    {

        double roundFactor = round ? 0.5d : 0.0;
        if (precision <= 0)
        {
            return Long.toString((long) Math.floor(value * 100 + roundFactor)) + '%';
        }
        else
        {
            //technically we should make sure that the value * the shiftFactor does not exceed
            // Double.MAX_VALUE.  But that is an unlikely use case, so we favor performance over safety
            precision = Math.min(5, precision);
            double shiftFactor = MathUtils.pow(10, precision);
            long shifted = (long) Math.floor(value * 100 * shiftFactor + roundFactor);
            double result = shifted / (shiftFactor);
            return Double.toString(result) + '%';
        }
    }


    public static double constrainBetweenOneAndNegOne(double d)
    {
        if (d > 1)
        {
            return 1;
        }
        else if (d < -1)
        {
            return -1;
        }
        else
        {
            return d;
        }
    }


    public static int constrainBetweenZeroAnd360(int value)
    {
        if (value > 360)
        {
            return 360;
        }
        else if (value < 0)
        {
            return 0;
        }
        else
        {
            return value;
        }
    }


    public static double constrainBetweenZeroAnd360(double value)
    {
        if (value > 360)
        {
            return 360;
        }
        else if (value < 0)
        {
            return 0;
        }
        else
        {
            return value;
        }
    }


    public static int constrainValue(int value, int min, int max)
    {
        if (value > max)
        {
            return max;
        }
        else if (value < min)
        {
            return min;
        }
        else
        {
            return value;
        }
    }


    public static double constrainValue(double value, int min, int max)
    {
        if (value > max)
        {
            return max;
        }
        else if (value < min)
        {
            return min;
        }
        else
        {
            return value;
        }
    }


    public static double constrainValue(double value, double min, double max)
    {
        if (value > max)
        {
            return max;
        }
        else if (value < min)
        {
            return min;
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range of 0 to 1, retaining the sign when returned. For example, if a value
     * of -2 is received with, a value of -1 is returned.
     *
     * @param value The value to constrain
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteValueToOne(double value)
    {
        if (Math.abs(value) > 1)
        {
            return matchSign(1, value);
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range of 0 to 1, retaining the sign when returned. For example, if a value
     * of -2 is received with, a value of -1 is returned.
     *
     * @param value The value to constrain
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteMaxValueToOne(float value)
    {
        if (Math.abs(value) > 1)
        {
            return matchSign(1, value);
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
     * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Min and Max values
     * should be positive values.
     *
     * @param value The value to constrain
     * @param min   the inclusive absolute min value
     * @param max   the inclusive absolute max value
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteMaxValue(double value, int max)
    {
        if ((max < 0) ) {throw new IllegalArgumentException("Max value passed into constrainAbsoluteMaxValue' method must both be positive numbers");}

        if (Math.abs(value) > max)
        {
            return matchSign(max, value);
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
     * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Min and Max values
     * should be positive values.
     *
     * @param value The value to constrain
     * @param min   the inclusive absolute min value
     * @param max   the inclusive absolute max value
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteMaxValue(float value, float max)
    {
        if ((max < 0)) {throw new IllegalArgumentException("Max value passed into constrainAbsoluteMaxValue' method must both be positive numbers");}

        if (Math.abs(value) > max)
        {
            return matchSign(max, value);
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
     * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Min and Max values
     * should be positive values.
     *
     * @param value The value to constrain
     * @param min   the inclusive absolute min value
     * @param max   the inclusive absolute max value
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteMaxValue(float value, int max)
    {
        if ((max < 0)) {throw new IllegalArgumentException("Max value passed into constrainAbsoluteMaxValue' method must both be positive numbers");}

        if (Math.abs(value) > max)
        {
            return matchSign(max, value);
        }
        else
        {
            return value;
        }
    }


    /**
     * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
     * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Min and Max values
     * should be positive values.
     *
     *
     * @param value The value to constrain
     * @param max   the inclusive absolute max value
     *
     * @return the constrained value value
     */
    public static double constrainAbsoluteMaxValue(double value, double max)
    {
        if ((max < 0)) {throw new IllegalArgumentException("Max value passed into constrainAbsoluteMaxValue' method must both be positive numbers");}

        if (Math.abs(value) > max)
        {
            return matchSign(max, value);
        }
        else
        {
            return value;
        }
    }


    public static double matchSign(double value, double valueToMatch)
    {
        if (valueToMatch <= 0.0D)
        {
            return -Math.abs(value);
        }
        else
        {
            return Math.abs(value);
        }
    }


    public static double matchSign(float value, float valueToMatch)
    {
        if (valueToMatch <= 0.0D)
        {
            return -Math.abs(value);
        }
        else
        {
            return Math.abs(value);
        }
    }


    /**
     * Determines if two values have the same sign or not. If one of the values is zero, the method will always return true,
     *
     * @param first  first value
     * @param second second value
     *
     * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
     */
    public static boolean haveSameSign(double first, double second)
    {
        return first == 0 || second == 0 || (first > 0 && second > 0) || (first < 0 && second < 0);
    }


    /**
     * Determines if two values have the same sign or not. If one of the values is zero, the method will always return true,
     *
     * @param first  first value
     * @param second second value
     *
     * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
     */
    public static boolean haveSameSign(int first, int second)
    {
        return first == 0 || second == 0 || (first > 0 && second > 0) || (first < 0 && second < 0);
    }


    /**
     * Floors a double value to the nearest X percent. For example, for x = 5: <ul> <li>0.025 = 0.0</li> <li>0.144 = 0.1</li> <li>0.151 = 0.15</li> <li>0.049 = 0.0</li> <li>0.394 =
     * 0.35</li> </ul>
     *
     * @param d the number to floor
     * @param x the target percentage increments. For example, for 5% (i.e. 0.05) it would be 5.
     *
     * @return the floored value
     */
    public static double floorToXPercent(double d, int x)
    {
        double percent = d * 100;
        final double v = percent - (percent % x);
        return v / 100;
    }


    /**
     * Rounds a double value up or down to the nearest X percent. For example, for x = 5: <ul> <li>0.024 = 0.0</li> <li>0.025 = 0.05</li> <li>0.144 = 0.15</li> <li>0.151 =
     * 0.15</li> <li>0.049 = 0.05</li> <li>0.394 = 0.4</li> </ul>
     *
     * @param d the number to round
     * @param x the target percentage increments. For example, for 5% (i.e. 0.05) it would be 5.
     *
     * @return the rounded value
     */
    public double roundToXPercent(double d, int x)
    {
        double percent = d * 100;
        int q = (int) (percent / x);
        double r = percent % x;
        int roundFactor = (r < (double) x / 2) ? 0 : x;
        int rounded = (q * x) + roundFactor;
        double value = (double) rounded / 100;
        return value;
    }


    public static double roundToNPlaces(double value, int n)
    {
        /* The sunspot VM does no have the pow method available - commenting out for now since this is not a critical method */
//        n = Math.max(n, 0);
//        final long factor = (long) Math.pow(10, n);
//        return (double) Math.round(value * factor) / factor;
        return value;
    }


    public static float roundToNPlaces(float value, int n)
    {
         /* The sunspot VM does no have the pow method available - commenting out for now since this is not a critical method */
//        n = Math.max(n, 0);
//        final long factor = (long) Math.pow(10, n);
//        return (float) Math.round(value * factor) / factor;
        return value;
    }
}
