package frc3838.Y2014.controls;


import java.util.Vector;

import frc3838.Y2014.utils.LOG;



/** @noinspection UseOfObsoleteCollectionType */
public class Buttons
{
    private static final Vector assigned = new Vector(44);

//    public static final NamedJoystickButton TURRET_MOTOR_TOGGLE = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getHighHatTopButton3());
//    public static final NamedJoystickButton TURRET_MOTOR_STOP = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getHighHatBottomButton2());
//    public static final NamedJoystickButton TURRET_MOTOR_FORWARD = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getHighHatRightButton5());
//    public static final NamedJoystickButton TURRET_MOTOR_REVERSE = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getHighHatLeftButton4());


    public static final NamedJoystickButton TURRET_STOP = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getHighHatTopButton3());
    public static final NamedJoystickButton TURRET_MOVE_TO_DASHBOARD_POSITION = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getHighHatBottomButton2());


    public static final NamedJoystickButton MOVE_TO_180 = assignButton(JoystickDef.getDriverNonArcadeJoystickDef().getRightGroupLowerButton10());
    public static final NamedJoystickButton MOVE_TO_120 = assignButton(JoystickDef.getDriverNonArcadeJoystickDef().getRightGroupUpperBottomButton11());

    public static final NamedJoystickButton MOVE_TO_90 = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getLowerGroupLeftButton8());
    public static final NamedJoystickButton MOVE_TO_270 = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getLowerGroupRightButton9());


    public static final NamedJoystickButton FLIPPER_RIGHT_OPEN = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getRightGroupUpperBottomButton11());
    public static final NamedJoystickButton FLIPPER_RIGHT_CLOSE = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getRightGroupLowerButton10());
    public static final NamedJoystickButton FLIPPER_LEFT_OPEN = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getLeftGroupUpperButton6());
    public static final NamedJoystickButton FLIPPER_LEFT_CLOSE = assignButton(JoystickDef.getArcadeDriveAssignedJoystickDef().getLeftGroupLowerButton7());


    public static final NamedJoystickButton FIRE = assignButton(JoystickDef.opsRight.getTriggerButton1());
    
    // TODO: REMOVE BEFORE MATCH
    public static final NamedJoystickButton STOP_TURRET_1 = assignButton(JoystickDef.driverLeft.getTriggerButton1());
    public static final NamedJoystickButton STOP_TURRET_2 = assignButton(JoystickDef.driverRight.getTriggerButton1());
    public static final NamedJoystickButton STOP_TURRET_3 = assignButton(JoystickDef.opsLeft.getTriggerButton1());


    public static int getNumberOfAssignedButtons() {return assigned.size();}


    /**
     * Method that verifies there are no duplicate button assignments, and returns the passed in button as a convenience. This allows for a construct such as:
     * <pre>
     *     NamedJoystickButton HORN = assignButton(JoystickDef.driverLeft.getHighHatLeftButton4());
     * </pre>
     *
     * @param namedJoystickButton the button to verify has not already been assigned
     *
     * @return the button passed in; returned as a convenience to allow for a construct
     * as {@code NamedJoystickButton HORN = assignButton(JoystickDef.driverLeft.getHighHatLeftButton4());}
     */
    static NamedJoystickButton assignButton(NamedJoystickButton namedJoystickButton)
    {
        boolean alreadyAssigned = assigned.contains(namedJoystickButton);
        if (alreadyAssigned)
        {
            String msg = namedJoystickButton + " is already assigned";
            LOG.error(msg);
            LOG.error("***********************************");
            LOG.error("*** DUPLICATE BUTTON ASSIGNMENT ***");
            LOG.error("***                             ***");
            LOG.error("*** DUPLICATE BUTTON ASSIGNMENT ***");
            LOG.error("***                             ***");
            LOG.error("*** DUPLICATE BUTTON ASSIGNMENT ***");
            LOG.error("***********************************");
            LOG.error(msg);
            throw new IllegalStateException(msg);
        }
        else
        {
            assigned.addElement(namedJoystickButton);
        }

        return namedJoystickButton;
    }


    //         JOYSTICK BUTTON LAYOUT
    // **************************************
    // *              (Trigger)             *
    // *          .................         *
    // *          :               :         *
    // * +---+    :     +---+     :   +---+ *
    // * | 6 |    . +-+ | 3 | +-+ :   + 11| *
    // * +---+    : |4| +---+ |5| :   +---+ *
    // *          : +-+ +---+ +-+ :         *
    // * +---+    :     | 2 |     :   +---+ *
    // * | 7 |    :     +---+     :   | 10| *
    // * +---+    :...............:   +---+ *
    // * Left          Stick          Right *
    // * Group      +---+  +---+      Group *
    // *            | 8 |  | 9 |            *
    // *            +---+  +---+            *
    // *            Bottom Group            *
    // **************************************

    //// CREATING BUTTONS
    // One type of button is a joystick button which is any button on a joystick.
    // You create one by telling it which joystick it's on and which button
    // number it is.
    // Joystick stick = new Joystick(port);
    // Button button = new JoystickButton(stick, buttonNumber);

    // Another type of button you can create is a DigitalIOButton, which is
    // a button or switch hooked up to the cypress module. These are useful if
    // you want to build a customized operator interface.
    // Button button = new DigitalIOButton(1);

    // There are a few additional built in buttons you can use. Additionally,
    // by subclassing Button you can create custom triggers and bind those to
    // commands the same as any other Button.

    //// TRIGGERING COMMANDS WITH BUTTONS
    // Once you have a button, it's trivial to bind it to a button in one of
    // three ways:

    // Start the command when the button is pressed and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenPressed(new ExampleCommand());

    // Run the command while the button is being held down and interrupt it once
    // the button is released.
    // button.whileHeld(new ExampleCommand());

    // Start the command when the button is released  and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenReleased(new ExampleCommand());

}
