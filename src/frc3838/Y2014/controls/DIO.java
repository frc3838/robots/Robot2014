package frc3838.Y2014.controls;


public class DIO
{

    //Encoder test - on Digital I/O 13 (ch. A) and 14 (ch. B)
    public static final int LEFT_ENCODER_CH_A = 13;   //Ch A Yellow Left
    public static final int LEFT_ENCODER_CH_B = 14;  //Ch B Blue Left

    public static final int RIGHT_ENCODER_CH_A = 11; //Yellow Right 11
    public static final int RIGHT_ENCODER_CH_B = 12; //Blue Right 12


    public static final int FLIPPER_LEFT_SIDE_STOP_CLOSING_LIMIT_SW = 7;
    public static final int FLIPPER_LEFT_FRONT_STOP_OPENING_LIMIT_SW =  8;

    public static final int FLIPPER_RIGHT_SIDE_STOP_CLOSING_LIMIT_SW = 9;
    public static final int FLIPPER_RIGHT_FRONT_STOP_OPENING_LIMIT_SW =  10;

    public static final int COMPRESSOR_PRESSURE_SWITCH_CHANNEL = 1;


    private DIO() {}
}
