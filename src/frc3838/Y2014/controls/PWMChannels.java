package frc3838.Y2014.controls;


public final class PWMChannels
{


    public static final int RIGHT_DRIVE_MOTOR_A = 1;
    public static final int RIGHT_DRIVE_MOTOR_B = 2;
    
    public static final int LEFT_DRIVE_MOTOR_A = 3;
    public static final int LEFT_DRIVE_MOTOR_B = 4;


    public static final int TURRET_MOTOR = 5;
    
    public static final int RIGHT_FLIPPER_MOTOR = 6;
    public static final int LEFT_FLIPPER_MOTOR = 7;
    
    public static final int VAC_MOTORS = 8;
    
    
    private PWMChannels() {}
}
