package frc3838.Y2014.controls;


public class Relays
{
    public static final int COMPRESSOR_RELAY_CHANNEL = 1;

    public static final int LED_RING_LIGHT = 2;
    
    public static final int FIRING_PIN_SOLENOID = 3;
    
    private Relays() {}
}
