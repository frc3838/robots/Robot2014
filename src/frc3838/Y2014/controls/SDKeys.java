package frc3838.Y2014.controls;


public final class SDKeys
{


    public static final String DRIVE_SENSITIVITY = "Drive Sensitivity";
    public static final String AUTONOMOUS_MODE_CHOOSER = "Autonomous Mode Chooser";
    public static final String TURRET_MOTOR_SPEED_CONTROL = "TurretMotorSpeedControl";

    public static final String TURRET_POT_RAW_VALUE = "TurretPotRawValue";
    public static final String TURRET_POT_RAW_STRING_VALUE = "TurretPotRawStringValue";
    public static final String TURRET_POT_RAW_MIN_VALUE = "TurretPotRawMinValue";
    public static final String TURRET_POT_RAW_MAX_VALUE = "TurretPotRawMaxValue";

    public static final String TURRET_COMPASS_DISPLAY_VALUE = "TurretCompassDisplayValue";
    public static final String TURRET_COMPASS_NOMINAL_VALUE = "TurretNominalValue";


    public static final String TURRET_POT_PID_VALUE = "TurretPotPIDValue";
    public static final String TURRET_POT_PID_STRING_VALUE = "TurretPotPIDStringValue";

    public static final String TURRET_MOTOR_STATUS = "TurretMotorStatus";

    public static final String PID_TWEAKER_P = "PidTweakerP";
    public static final String PID_TWEAKER_I = "PidTweakerI";
    public static final String PID_TWEAKER_D = "PidTweakerD";
    public static final String PID_TWEAKER_FF = "PidTweakerFF";
    public static final String PID_TWEAKER_Set = "PidTweakerSet";
    public static final String PID_TWEAKER_ENABLED = "EnableTurretPID";
    public static final String PID_STATUS_STRING = "PIDStatusString";
    public static final String PID_OUTPUT = "PidOutput";
    public static final String DRIVE_CONTROL_MODE = "Drive Control Mode";

    public static final String LEFT_ENCODER_DISTANCE_TRAVELED = "Left Distance Traveled";
    public static final String RIGHT_ENCODER_DISTANCE_TRAVELED = "Right Distance Traveled";

    public static final String LEFT_ENCODER_COUNT = "Left Encoder Count";
    public static final String RIGHT_ENCODER_COUNT = "Right Encoder Count";

    public static final String LEFT_ENCODER_STATUS = "Left Encoder Status";
    public static final String RIGHT_ENCODER_STATUS = "Right Encoder Status";

    public static final String LEFT_ENCODER = "Left Encoder";
    public static final String RIGHT_ENCODER = "Right Encoder";
    public static final String PID_REQUESTED_OUTPUT = "PIDRequestedOutput";
    public static final String PID_OUTPUT_CUTOFF = "PIDOutputCutOff";

    public static final String FLIPPER_MOTOR_SPEED = "FlipperMotorSpeedSetting";

    public static final String TURRET_POSITION_TARGET = "TurretPositionTarget";
    public static final String PEEK_APPROACH_ANGLE_DELTA = "PeekApproachAngleDelta";
    public static final String PEEK_ZONE_MAX_SPEED = "PeekZoneMaxValue";
    public static final String MAX_TURRET_SPEED = "MaxTurretSpeed";


    private SDKeys() { }
}
