package frc3838.Y2014;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2014.commands.camera.ToggleLedRingLightCommand;
import frc3838.Y2014.commands.drive.ResetDriveEncoder;
import frc3838.Y2014.commands.flipper.CloseFlipperCommand;
import frc3838.Y2014.commands.flipper.OpenFlipperCommand;
import frc3838.Y2014.commands.flipper.StopFlipperMotorCommand;
import frc3838.Y2014.commands.shooter.FireCommand;
import frc3838.Y2014.commands.shooter.FireSequenceCommandGroup;
import frc3838.Y2014.commands.shooter.RetractCommand;
import frc3838.Y2014.commands.turret.PositionTurretTo120Command;
import frc3838.Y2014.commands.turret.PositionTurretTo180Command;
import frc3838.Y2014.commands.turret.PositionTurretTo270Command;
import frc3838.Y2014.commands.turret.PositionTurretTo90Command;
import frc3838.Y2014.commands.turret.PositionTurretToDashboardCommand;
import frc3838.Y2014.commands.turret.StopAndDisableTurretEnabledCommand;
import frc3838.Y2014.commands.vac.VacCommandsFactory;
import frc3838.Y2014.controls.Buttons;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.controls.JoystickDef;
import frc3838.Y2014.subsystems.flipper.FlipperSubsystem;
import frc3838.Y2014.utils.LOG;



/** @noinspection FieldCanBeLocal */
public class OI2014
{
    public OI2014()
    {
        LOG.debug("Entering OI2014 Constructor - calling various initXyzControls() methods");


        //Call the various initFooControls() methods
        initJoySticks();
        initButtonsClass();
        initDriveTrain();
        initVacSystemControls();
        initShooterControls();
        initTurretPositionButtons();
        initTurretDirectRunButtons();
        initFlipperControlButtons();
        initCameraControls();


        LOG.debug("Exiting OI2014 Constructor");
    }


    private void initCameraControls()
    {
        try
        {
            if (EM.isLedRingLightSubsystemEnabled)
            {
                SmartDashboard.putData("Toggle Led", new ToggleLedRingLightCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.OI2014.initCameraControls()", e);
        }


    }


    private void initShooterControls()
    {
        try
        {
            if (EM.isShooterSubsystemEnabled)
            {
                if (Robot2014.IN_DEBUG_MODE)
                {
                    SmartDashboard.putData("FIRE!", new FireSequenceCommandGroup());
                    SmartDashboard.putData("FireCommand", new FireCommand());
                    SmartDashboard.putData("RetractCommand", new RetractCommand());       
                }
                Buttons.FIRE.whenPressed(new FireSequenceCommandGroup());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.OI2014.initShooterControls()", e);
        }


    }


    private void initVacSystemControls()
    {
        try
        {
            if(EM.isVacSubsystemEnabled)
            {
                if (Robot2014.IN_DEBUG_MODE)
                {
                    SmartDashboard.putData("Start Vac", VacCommandsFactory.getStartVacCommand());
                    SmartDashboard.putData("Stop Vac", VacCommandsFactory.getStopVacCommand());
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.OI2014.initVacSystemControls()", e);
        }


    }


    private void initJoySticks()
    {
        try
        {
            JoystickDef.init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the Joysticks: " + e.toString(), e);
        }
    }


    private void initButtonsClass()
    {
        try
        {
            //We simply need to get a reference to all the Joysticks to ensure the static initializer runs
            Buttons.getNumberOfAssignedButtons();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in OI2014.initButtonsClass()", e);
        }
    }


    private void initDriveTrain()
    {
        try
        {
            if (EM.isDriveTrainSubsystemEnabled)
            {
                if (EM.isDriveTrainSubsystemEncodersEnabled)
                {
                    SmartDashboard.putData("ResetEncoder", new ResetDriveEncoder());
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.OI2014.initDriveTrain()", e);
        }

    }



    private void initTurretPositionButtons()
    {
        try
        {
            if (EM.isTurretSubsystemEnabled)
            {
                Buttons.MOVE_TO_180.whenPressed(new PositionTurretTo180Command());
                Buttons.MOVE_TO_120.whenPressed(new PositionTurretTo120Command());

                Buttons.MOVE_TO_90.whenPressed(new PositionTurretTo90Command());
                Buttons.MOVE_TO_270.whenPressed(new PositionTurretTo270Command());

                final PositionTurretToDashboardCommand positionCommand = new PositionTurretToDashboardCommand();
                Buttons.TURRET_MOVE_TO_DASHBOARD_POSITION.whenPressed(positionCommand);
                SmartDashboard.putData("PositionTo", positionCommand);

                if (Robot2014.IN_DEBUG_MODE)
                {
                    // TODO: REMOVE BEFORE MATCH (and move one instance to its final position)
                    Buttons.TURRET_STOP.whenPressed(new StopAndDisableTurretEnabledCommand());
                    Buttons.STOP_TURRET_1.whenPressed(new StopAndDisableTurretEnabledCommand());
                    Buttons.STOP_TURRET_2.whenPressed(new StopAndDisableTurretEnabledCommand());
                    Buttons.STOP_TURRET_3.whenPressed(new StopAndDisableTurretEnabledCommand());
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.OI2014.initTurretPositionButtons()", e);
        }
    }


    private void initFlipperControlButtons()
    {
        try
        {
            if (EM.isFlippersSubsystemEnabled)
            {

                OpenFlipperCommand rightOpenCommand = new OpenFlipperCommand(FlipperSubsystem.getRightInstance());
                CloseFlipperCommand rightCloseCommand = new CloseFlipperCommand(FlipperSubsystem.getRightInstance());

                OpenFlipperCommand leftOpenCommand = new OpenFlipperCommand(FlipperSubsystem.getLeftInstance());
                CloseFlipperCommand leftCloseCommand = new CloseFlipperCommand(FlipperSubsystem.getLeftInstance());

                StopFlipperMotorCommand stopRightFlipper = new StopFlipperMotorCommand(FlipperSubsystem.getRightInstance());
                StopFlipperMotorCommand stopLeftFlipper = new StopFlipperMotorCommand(FlipperSubsystem.getLeftInstance());

                Buttons.FLIPPER_RIGHT_OPEN.whenPressed(rightOpenCommand);
                Buttons.FLIPPER_RIGHT_CLOSE.whenPressed(rightCloseCommand);
                Buttons.FLIPPER_LEFT_OPEN.whenPressed(leftOpenCommand);
                Buttons.FLIPPER_LEFT_CLOSE.whenPressed(leftCloseCommand);

                if (Robot2014.IN_DEBUG_MODE)
                {
                    // TODO: REMOVE BEFORE MATCH - test/debug code
                    SmartDashboard.putData("OpenRFC", rightOpenCommand);
                    SmartDashboard.putData("CloseRFC", rightCloseCommand);
                    SmartDashboard.putData("OpenLFC", leftOpenCommand);
                    SmartDashboard.putData("CloseLFC", leftCloseCommand);

                    SmartDashboard.putData("STOP_R", stopRightFlipper);
                    SmartDashboard.putData("STOP_L", stopLeftFlipper);
                }

            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.OI2014.initFlipperControlButtons()", e);
        }

    }


    private void initTurretDirectRunButtons()
    {
        try
        {
            if (EM.isTurretDirectSubsystemEnabled)
            {
                LOG.debug("Configuring Turret Run Buttons in OI2014");
//                Buttons.TURRET_MOTOR_FORWARD.whenPressed(new StartTurretDirectMotorForwardCommand());
//                Buttons.TURRET_MOTOR_REVERSE.whenPressed(new StartTurretDirectMotorReverseCommand());
//                Buttons.TURRET_MOTOR_STOP.whenPressed(new StopTurretDirectMotorCommand());
//                Buttons.TURRET_MOTOR_TOGGLE.whenPressed(new ToggleTurretDirectMotorCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.OI2014.initTurretDirectRunButtons()", e);
        }
    }
}

