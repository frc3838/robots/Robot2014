package frc3838.Y2014.subsystems;


import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.controls.Relays;
import frc3838.Y2014.utils.LOG;



public class LedRingLightSubsystem extends Subsystem
{

    private static final LedRingLightSubsystem singleton = new LedRingLightSubsystem();
    private static Relay ledRingRelay;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new LedRingLightSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     LedRingLightSubsystem subsystem = new LedRingLightSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     LedRingLightSubsystem subsystem = LedRingLightSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static LedRingLightSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     LedRingLightSubsystem subsystem = new LedRingLightSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     LedRingLightSubsystem subsystem = LedRingLightSubsystem.getInstance();
     * </pre>
     */
    private LedRingLightSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in LedRingLightSubsystem() constructor", e);
        }
    }


    private void init()
    {
        if (isEnabled())
        {
            try
            {
                LOG.debug("Initializing LedRingLightSubsystem");
                ledRingRelay = new Relay(Relays.LED_RING_LIGHT);
                ledRingRelay.setDirection(Relay.Direction.kForward);
                LOG.debug("LedRingLightSubsystem initialization completed successfully");

            }
            catch (Exception e)
            {
                EM.isLedRingLightSubsystemEnabled = false;
                LOG.error("An exception occurred in LedRingLightSubsystem.init()", e);
            }
        }
        else
        {
            LOG.info("LedRingLightSubsystem is disabled and will not be initialized");
        }
    }

    public void turnLedRingLightOn()
    {
        ledRingRelay.set(Relay.Value.kOn);
    }
    
    public void turnLedRingLightOff()
    {
        ledRingRelay.set(Relay.Value.kOff);
    }
    
    public void toggleLedRingLight()
    {
        if (ledRingRelay.get().equals(Relay.Value.kOn))
        {
            turnLedRingLightOff();
        }
        else
        {
            turnLedRingLightOn();
        }
    }
  
    public void initDefaultCommand()
    {
        //no op - no default command at this time
        //setDefaultCommand(new MyCommand());
    }


    public boolean isEnabled()
    {
        return EM.isLedRingLightSubsystemEnabled;
    }
}
