package frc3838.Y2014.subsystems.motors;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Vector;



/** @noinspection UseOfObsoleteCollectionType, UnusedDeclaration */
public class Side
{
    private String name;
    public final int ordinal;
    private Side previous;
    private Side next;
    private static Side first = null;
    private static Side last = null;
    private static int upperBound = 0;
    private static Hashtable values = new Hashtable();
    private static Hashtable ordinalLookup = new Hashtable();

    public static final Side LEFT = new Side("LEFT");
    public static final Side RIGHT = new Side("RIGHT");

    //Lazy initialized collection objects
    private static Vector vector;
    private static Side[] array;
    private static String[] nameArray;


    /**
     * Constructs a new Side enumeration instance (i.e. constant). This constructor is private so that outside class cannot add any enumeration instances.
     *
     * @param name the name of the enumeration instance (i.e. constant), typically in all uppercase per standard naming conventions
     */
    private Side(String name)
    {
        this.name = name;
        ordinal = upperBound++;
        if (first == null) first = this;
        if (last != null)
        {
            previous = last;
            last.next = this;
        }
        last = this;
        values.put(name, this);
        ordinalLookup.put(Integer.valueOf(ordinal), this);
    }


    /**
     * Returns an {@link Enumeration} of all {@code Side}s.
     *
     * @return an Enumeration of all {@code Side}s
     */
    public static Enumeration getEnumeration()
    {
        return new Enumeration()
        {
            private Side current = first;


            public boolean hasMoreElements()
            {
                return current != null;
            }


            public Object nextElement()
            {
                if (current == null)
                {
                    throw new NoSuchElementException("There are no more elements in the Enumeration");
                }
                Side theNextElement = current;
                current = current.next();
                return theNextElement;
            }
        };
    }


    public static Vector asVector()
    {
        if (vector == null)
        {
            vector = new Vector(size());
            Enumeration enumeration = getEnumeration();
            while (enumeration.hasMoreElements())
            {
                Side item = (Side) enumeration.nextElement();
                vector.addElement(item);
            }
        }
        return vector;
    }


    /**
     * Returns an array of all {@code Side}s.
     *
     * @return an array of all {@code Side}s
     */
    public static Side[] asArray()
    {
        if (array == null)
        {
            array = new Side[size()];
            Enumeration enumeration = getEnumeration();
            int index = 0;
            while (enumeration.hasMoreElements())
            {
                Side item = (Side) enumeration.nextElement();
                array[index++] = item;
            }
        }
        return array;
    }


    /**
     * An array of the names of all {@code Side}s.
     *
     * @return an array of the names of all {@code Side}s.
     */
    public static String[] asArrayOfNames()
    {
        if (nameArray == null)
        {
            nameArray = new String[size()];
            Enumeration enumeration = getEnumeration();
            int index = 0;
            while (enumeration.hasMoreElements())
            {
                Side item = (Side) enumeration.nextElement();
                nameArray[index++] = item.getName();
            }
        }
        return nameArray;
    }


    /**
     * Indicates whether some other object is &quot;equal to&quot; this one. <p> The <code>equals</code> method implements an equivalence relation on non-null
     * object references: <ul> <li>It is <i>reflexive</i>: for any non-null reference value <code>x</code>, <code>x.equals(x)</code> should return
     * {@code true}. <li>It is <i>symmetric</i>: for any non-null reference values <code>x</code> and <code>y</code>, <code>x.equals(y)</code> should
     * return {@code true} if and only if <code>y.equals(x)</code> returns {@code true}. <li>It is <i>transitive</i>: for any non-null reference
     * values <code>x</code>, <code>y</code>, and <code>z</code>, if <code>x.equals(y)</code> returns {@code true} and <code>y.equals(z)</code> returns
     * {@code true}, then <code>x.equals(z)</code> should return {@code true}. <li>It is <i>consistent</i>: for any non-null reference values
     * <code>x</code> and <code>y</code>, multiple invocations of {@code x.equals(y)} consistently return {@code true} or consistently return
     * {@code false}, provided no information used in <code>equals</code> comparisons on the objects is modified. <li>For any non-null reference value
     * <code>x</code>, <code>x.equals(null)</code> should return {@code false}. </ul>
     *
     * @param o the reference object with which to compare.
     *
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Side castedObject = (Side) o;
        return name.equals(castedObject.name);
    }


    /**
     * An overloading of the standard {@link #equals(Object)} method that takes a {@code String} representing
     * the name of an enumerated instance. Use of this method is semantically equivalent to:
     * <pre>
     * Side.FOO.equals(Side.valueOf(myLoggingLevel);
     * </pre>
     * and
     * <pre>
     * Side.FOO.getName().equals(myLoggingLevel.getName());
     * </pre>
     * It is provided as a convenience method for situation where the caller is dealing with a String that maps to a {@code Side}.
     *
     * @param name the name of the enumeration instance (i.e. constant) with which to compare
     *
     * @return {@code true} if this object is the same name as the name argument; {@code false} otherwise.
     */


    public boolean equals(String name)
    { return this.name.equals(name); }


    /**
     * An overloading of the standard {@link #equals(Object)} method that takes an {@code int} representing the ordinal
     * of an enumerated instance. Use of this method is semantically equivalent to:
     * <pre>
     * Side.FOO.ordinal == myLoggingLevel.ordinal;
     * </pre>
     *
     * @param ordinal the ordinal value with which to compare to
     *
     * @return{@code true} if this object's ordinal is the same as the ordinal argument; {@code false} otherwise.
     */
    public boolean equals(int ordinal)
    { return this.ordinal == ordinal; }


    public int hashCode() { return name.hashCode(); }


    /**
     * Returns the name of the enumeration instance (i.e. constant). For {@code Side.FOO} this method would return &quot;FOO&quot;.
     *
     * @return the name of the enumeration instance (i.e. constant)
     */
    public String getName()
    { return this.name; }


    /**
     * The size of the enumeration set. In other words, how may enumeration instances (i.e. constants) exist.
     *
     * @return size of the enumeration set
     */
    public static int size()
    { return values.size(); }


    /**
     * Gets the first <tt>Side<tt> in the enumeration set.
     *
     * @return the first <tt>Side<tt> in the enumeration set
     */
    public static Side first()
    { return first; }


    /**
     * Gets the last <tt>Side<tt> in the enumeration set.
     *
     * @return the last <tt>Side<tt> in the enumeration set.
     */
    public static Side last()
    { return last; }


    /**
     * Gets a {@code Side} represented name. May return {@code null} if the name received does not represent a valid enumeration instance.
     *
     * @param name the name to of the {@code Side} to get
     *
     * @return the {@code Side} represented by the name argument, or {@code null} if there is no {@code Side} represented by the received name
     */
    public static Side valueOf(String name)
    { return (Side) values.get(name); }


    /**
     * Gets a {@code Side} represented by an ordinal. May return {@code null} if the ordinal received does not represent a valid enumeration instance.
     *
     * @param ordinal the ordinal to of the {@code Side} to get
     *
     * @return the {@code Side} represented by the ordinal argument, or {@code null} if there is no {@code Side} represented by the received ordinal
     */
    public static Side valueOf(int ordinal)
    { return (Side) ordinalLookup.get(Integer.valueOf(ordinal)); }


    /**
     * Gets the previous {@code Side} in the enumeration. If this instance is the first {@code Side}, {@code null} is returned.
     *
     * @return the previous {@code Side} or {@code null} if this is the first {@code Side} in the enumeration
     */
    public Side previous()
    { return this.previous; }


    /**
     * Gets the next {@code Side} in the enumeration. If this instance is the last {@code Side}, {@code null} is returned.
     *
     * @return the next {@code Side} or {@code null} if this is the last {@code Side} in the enumeration
     */
    public Side next()
    { return this.next; }


    /**
     * Returns the name of the enumeration instance (i.e. constant). For {@code Side.FOO} this method would return &quot;FOO&quot;.
     *
     * @return the name of the enumeration instance (i.e. constant)
     */
    public String toString()
    { return this.name; }


    /**
     * Gets the ordinal of the enumeration instance (i.e. constant). This is a convenience method since the {@link #ordinal} property itself is public.
     *
     * @return the ordinal of the enumeration instance (i.e. constant)
     */
    public int getOrdinal()
    { return ordinal; }
}
