package frc3838.Y2014.subsystems;


import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.Victor;
import frc3838.Y2014.controls.AIO;
import frc3838.Y2014.controls.PWMChannels;



public class TurretComponents
{
    private static TurretComponents instance = new TurretComponents();



    public static final double potMaxVoltage = 5.0;

    //See AnalogPotentiometer constructor javadoc for info on the scale and offset
    public static final double scale = 360.0 / potMaxVoltage;
    public static final double offset = 0.0;

    private AnalogPotentiometer potentiometer;

    private Victor speedController;


    private TurretComponents()
    {
        potentiometer = new AnalogPotentiometer(AIO.TURRET_CONTROL_POTENTIOMETER, scale, offset);

        speedController = new Victor(PWMChannels.TURRET_MOTOR);
    }


    public static TurretComponents getInstance() { return instance; }


    public AnalogPotentiometer getPotentiometer()
    {
        return potentiometer;
    }


    public Victor getSpeedController()
    {
        return speedController;
    }
}
