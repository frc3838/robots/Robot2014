package frc3838.Y2014.subsystems.flipper;


import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2014.Robot2014;
import frc3838.Y2014.commands.flipper.UpdateBothFlippersStatusCommand;
import frc3838.Y2014.controls.DIO;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.controls.PWMChannels;
import frc3838.Y2014.controls.SDKeys;
import frc3838.Y2014.subsystems.motors.MotorOps;
import frc3838.Y2014.utils.LOG;



public class FlipperSubsystem extends Subsystem
{
    protected final static FlipperConfig rightConfig;
    protected final static FlipperConfig leftConfig;
    
    protected final static FlipperSubsystem rightInstance;
    protected final static FlipperSubsystem leftInstance;

    protected MotorOps motorOps;
    protected DigitalInput openingLimitSwitch;
    protected DigitalInput closingLimitSwitch;

    /* Static initializer block*/
    static 
    {
        rightConfig = new FlipperConfig();
        rightConfig.name = "RightFlipperSubsystem";
        rightConfig.inverseMotor= true;
        rightConfig.motorPwmChannel = PWMChannels.RIGHT_FLIPPER_MOTOR;
        rightConfig.openingLimitSwitchDioPort = DIO.FLIPPER_RIGHT_FRONT_STOP_OPENING_LIMIT_SW;
        rightConfig.closingLimitSwitchDioPort = DIO.FLIPPER_RIGHT_SIDE_STOP_CLOSING_LIMIT_SW;
        
        leftConfig = new FlipperConfig();
        leftConfig.name = "LeftFlipperSubsystem";
        leftConfig.inverseMotor= false;
        leftConfig.motorPwmChannel = PWMChannels.LEFT_FLIPPER_MOTOR;
        leftConfig.openingLimitSwitchDioPort = DIO.FLIPPER_LEFT_FRONT_STOP_OPENING_LIMIT_SW;
        leftConfig.closingLimitSwitchDioPort = DIO.FLIPPER_LEFT_SIDE_STOP_CLOSING_LIMIT_SW;
        
        rightInstance = new FlipperSubsystem(rightConfig);
        leftInstance = new FlipperSubsystem(leftConfig);
    }


    private double defalutSpeed;
    private Victor speedController;


    public FlipperSubsystem(FlipperConfig flipperConfig)
    {
        super(flipperConfig.name);
        init(flipperConfig);
    }


    public static FlipperSubsystem getRightInstance() { return rightInstance; }

    
    public static FlipperSubsystem getLeftInstance() { return leftInstance; } 
    

    private void init(FlipperConfig config)
    {
        if (isEnabled())
        {
            try
            {
                LOG.debug("Initializing " + config.name);
                speedController = new Victor(config.motorPwmChannel);
                motorOps = new MotorOps(speedController, config.inverseMotor);
                defalutSpeed = 0.5;
                motorOps.setSpeedWithoutModifyingRunState(defalutSpeed);
                
                openingLimitSwitch = new DigitalInput(config.openingLimitSwitchDioPort);
                closingLimitSwitch = new DigitalInput(config.closingLimitSwitchDioPort);

                updateLimitSwitchStatusDisplay();

                LOG.debug(config.name + " initialization completed successfully");
            }
            catch (Exception e)
            {
                EM.isFlippersSubsystemEnabled = false;
                LOG.error("An exception occurred in " + config.name + ".init()", e);
            }
        }
        else
        {
            LOG.info("FlipperSubsystem is disabled and will not be initialized");
        }

    }

    public void updateStatus()
    {
        updateLimitSwitchStatusDisplay();
    }
   

    private void updateLimitSwitchStatusDisplay()
    {
        if (Robot2014.IN_DEBUG_MODE)
        {
            SmartDashboard.putData(getName() + "Open Sw", openingLimitSwitch);
            SmartDashboard.putData(getName() + "Close Sw", closingLimitSwitch);
        }
    }


    public boolean isEnabled()
    {
        return EM.isFlippersSubsystemEnabled;
    }

    protected void initDefaultCommand()
    {
        setDefaultCommand(new UpdateBothFlippersStatusCommand());
    }


    public void startMotorOpening()
    {
        try
        {
            if (isEnabled())
            {
                final double speed = readFlipperMotorSpeedSetting();
                LOG.trace("Opening " + getName() + " at a speed of: " + speed);
                motorOps.setSpeed(speed);
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in " + getName() +".startMotorOpening()", e);
        }
    }
  
    public void startMotorClosing()
    {
        try
        {
            if (isEnabled())
            {
                final double speed = -readFlipperMotorSpeedSetting();
                LOG.trace("Closing " + getName() + " at a speed of: " + speed);
                motorOps.setSpeed(speed);
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in " + getName() +".startMotorClosing()", e);
        }
    }
    
    public void stopMotor()
    {
        try
        {
            if (isEnabled())
            {
//                motorOps.stop();
                speedController.set(0);
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in " + getName() + ".stopMotor()", e);
        }
    }
    
    
    public double readFlipperMotorSpeedSetting()
    {
        try
        {
            // TODO: REMOVE BEFORE MATCH - test/debug code - Change method to just return a hard coded value?
            final double speed = SmartDashboard.getNumber(SDKeys.FLIPPER_MOTOR_SPEED);
            LOG.trace("Flipper speed read as: " + speed);
            return speed;
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.subsystems.flipper.FlipperSubsystem.readFlipperMotorSpeedSetting()", e);
            return 0.4;
        }
    }


    public DigitalInput getOpeningLimitSwitch() { return openingLimitSwitch; }


    public DigitalInput getClosingLimitSwitch() { return closingLimitSwitch; }


    protected static class FlipperConfig
    {
        private String name;
        private int motorPwmChannel;
        private boolean inverseMotor = false;
        private int openingLimitSwitchDioPort;
        private int closingLimitSwitchDioPort;
    }


    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FlipperSubsystem that = (FlipperSubsystem) o;

        
        return this.getName().equals(that.getName());
    }


    public int hashCode()
    {
        return getName().hashCode();
    }
    
    
    public boolean isStopped()
    {
        return !motorOps.isRunning();
    }
    
    public boolean isRunning()
    {
        return motorOps.isRunning();
    }
}
