package frc3838.Y2014.subsystems;


import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.PIDSubsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2014.Robot2014;
import frc3838.Y2014.commands.turret.MonitorTurretCommand;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.controls.SDKeys;
import frc3838.Y2014.utils.LOG;
import frc3838.Y2014.utils.math.Math2;



/**
 * Subsystem extension of the WPI Lib {@link edu.wpi.first.wpilibj.command.PIDSubsystem} for controlling a subsystem that uses
 * a single <a href="http://en.wikipedia.org/wiki/PID_controller">proportional-integral-derivative controller (PID controller)</a>
 * (see {@link edu.wpi.first.wpilibj.PIDController}) almost constantly (for instance, an elevator which attempts to stay
 * at a constant height).
 *
 * <p>It provides some convenience methods to run an internal {@link edu.wpi.first.wpilibj.PIDController}.
 * It also allows access to the internal {@link edu.wpi.first.wpilibj.PIDController} in order to give total control
 * to the programmer.</p>
 */
public class TurretSubsystem extends PIDSubsystem
{

    private static final TurretSubsystem singleton = new TurretSubsystem();

    public static final double ANGLE_ADJUSTMENT_FACTOR = 7.0;
    public static final double MIN_POT_ANGLE = 90.0D;
    public static final double MAX_POT_ANGLE = 295.0D;

    public static final double MIN_NOMINAL_ANGLE = adjustPotAngleToNominalAngle(MIN_POT_ANGLE);
    public static final double MAX_NOMINAL_ANGLE = adjustPotAngleToNominalAngle(MAX_POT_ANGLE);

    protected static final int ROUNDING_PLACES = 1;

    public static final double CONFIGURED_P = 0.022;
    private static double p = CONFIGURED_P;
    private static double i = 0.0;
    private static double d = 0.0;
    private static double ff = 0.0;

    private double pidOutput = 0.0;
    private AnalogPotentiometer potentiometer;
    private SpeedController speedController;



    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use of a constructor such
     * as <tt>new TurretSubsystem()</tt> in order to use ensure only a single instance of a Subsystem is created. This
     * is known as a Singleton. For example, instead of doing this:
     * <pre>
     *     TurretSubsystem subsystem = new TurretSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     TurretSubsystem subsystem = TurretSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static TurretSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     TurretSubsystem subsystem = new TurretSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     TurretSubsystem subsystem = TurretSubsystem.getInstance();
     * </pre>
     */
    private TurretSubsystem()
    {
        super("TurretSubsystem", p, i, d, ff);
        //put initialization code in the init() method rather rather than here
        try
        {
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in TurretSubsystem() constructor", e);
        }

    }


    private void init()
    {
        if (EM.isTurretSubsystemEnabled)
        {
            try
            {
                LOG.debug("Initializing TurretSubsystem");

                if (Robot2014.IN_DEBUG_MODE)
                {
                    // TODO: REMOVE BEFORE MATCH
                    SmartDashboard.putBoolean(SDKeys.PID_TWEAKER_ENABLED, false);

                    SmartDashboard.putNumber(SDKeys.PID_OUTPUT_CUTOFF, 0.25);
                }
  
                potentiometer = TurretComponents.getInstance().getPotentiometer();
                speedController = TurretComponents.getInstance().getSpeedController();

            }
            catch (Exception e)
            {
                EM.isTurretSubsystemEnabled = false;
                LOG.error("An exception occurred in TurretSubsystem.init()", e);
            }
        }
        else
        {
            LOG.info("TurretSubsystem is disabled and will not be initialized");
        }
    }


    /** @deprecated - use setSetPoint() instead */
    public void setPositionTo(double setPoint)
    {
        setSetpoint(setPoint);
    }


    public void setSetpoint(double targetNominalAngle)
    {
        targetNominalAngle = constrainNominalAngle(targetNominalAngle);
        
        SmartDashboard.putNumber(SDKeys.PID_TWEAKER_Set, targetNominalAngle);
        final double potValue = adjustNominalAngleToAPotAngle(targetNominalAngle);
        LOG.debug("Positioning turret to Nominal Value " + targetNominalAngle + " potValue of " +  potValue);
        super.setSetpoint(potValue);
    }


    private static double constrainNominalAngle(double nominalAngle)
    {
        return Math2.roundToNPlaces(Math2.constrainValue(nominalAngle, MIN_NOMINAL_ANGLE, MAX_NOMINAL_ANGLE), ROUNDING_PLACES);
    }


    private static double constrainPotAngle(double potAngle)
    {
        return Math2.constrainValue(potAngle, MIN_POT_ANGLE, MAX_POT_ANGLE);
    }


    private static double adjustNominalAngleToAPotAngle(double targetNominalAngle)
    {
        final double adjusted = targetNominalAngle - ANGLE_ADJUSTMENT_FACTOR;
        return adjusted < 0 ? adjusted + 360 : adjusted;
    }


    private static double adjustPotAngleToNominalAngle(double potAngle)
    {
        final double adjusted = potAngle + ANGLE_ADJUSTMENT_FACTOR;
        return adjusted >= 360 ? adjusted - 360 : adjusted;
    }


    public boolean updatePIDEnabledStatusFromDashboard()
    {
        final boolean enableTurretPID = SmartDashboard.getBoolean(SDKeys.PID_TWEAKER_ENABLED, true);
        if (enableTurretPID)
        {
            enable();
        }
        else
        {
            disable();
        }
        return enableTurretPID;
    }

    public void initDefaultCommand()
    {
        setDefaultCommand(new MonitorTurretCommand());
    }


    protected double returnPIDInput()
    {
        // Return your input value for the PID loop
        // e.g. a sensor, like a potentiometer:
        // yourPot.getAverageVoltage() / kYourMaxVoltage;
        return potentiometer.get();
    }


    protected void usePIDOutput(double output)
    {
        // Use output to drive your system, like a motor
        // e.g. yourMotor.set(output);

        // TODO: REMOVE BEFORE MATCH
        if (Robot2014.IN_DEBUG_MODE)
        {
            SmartDashboard.putNumber(SDKeys.PID_REQUESTED_OUTPUT, output);
        }

        final double cutoff = readCutoff();
        pidOutput = Math.abs(output) < cutoff ? 0 : output;
        if (pidOutput == 0)
        {
            LOG.debug("PIDOutput cut off to zero");
        }
        else
        {
            final double delta = readApproachAngleDelta();
            final double currentPosition = getPotentiometerAdjustedValue();
            if (currentPosition >= 180 - delta && currentPosition <= 180 + delta)
            {
                final double maxSpeed = readPeekZoneMaxSpeed();
                LOG.debug("PIDOutput constrained to Peek Value of " + maxSpeed);
                pidOutput = Math2.constrainAbsoluteMaxValue(output, maxSpeed);
            }
            else
            {
                final double readMaxTurretSpeed = readMaxTurretSpeed();
                LOG.debug("Max Turret Speed read as " + readMaxTurretSpeed);
                pidOutput = Math2.constrainAbsoluteMaxValue(output, readMaxTurretSpeed);
                LOG.debug("Post constrainAbsoluteMaxValue, output of " + output + " constrained to pidOutput of " + pidOutput);
            }
        }
        LOG.debug("pidOutput = " + pidOutput);
//        speedController.pidWrite(0);
        speedController.pidWrite(pidOutput);

        SmartDashboard.putNumber(SDKeys.PID_OUTPUT, pidOutput);
        updateDisplay();
    }

    private static double readMaxTurretSpeed()
    {
        try
        {
            return SmartDashboard.getNumber(SDKeys.MAX_TURRET_SPEED);
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.subsystems.TurretSubsystem.readMaxTurretSpeed()", e);
            return 0.8;
        }
    }
    
    private static double readPeekZoneMaxSpeed()
    {
        try
        {
            final double maxSpeed = SmartDashboard.getNumber(SDKeys.PEEK_ZONE_MAX_SPEED);
            LOG.debug("PeekZoneMaxValue read as " + maxSpeed);
            return maxSpeed;
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.subsystems.TurretSubsystem.readPeekZoneMaxSpeed()", e);
            return 0.2;
        }
    }

    private static double readApproachAngleDelta()
    {
        try
        {
            final double delta = SmartDashboard.getNumber(SDKeys.PEEK_APPROACH_ANGLE_DELTA, 2);
            LOG.debug("PeekApproachAngleDelta read as " + delta);
            return Math2.roundToNPlaces(delta, 2);
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.subsystems.TurretSubsystem.readApproachAngleDelta()", e);
            return 2;
        }
    }

    
    
    
    private double readCutoff() {return Math2.roundToNPlaces(SmartDashboard.getNumber(SDKeys.PID_OUTPUT_CUTOFF), 3);}


    public void updateMonitoring()
    {
        updatePotentiometerDisplay();
        updatePIDEnabledStatusFromDashboard();
        updatePidTweakerValues();
        updateDisplay();
    }


    public void updateDisplay()
    {
        updateMotorSpeedDisplay();
        updatePotentiometerDisplay();
//        SmartDashboard.putData("PIDController", getPIDController());
        SmartDashboard.putNumber(SDKeys.PID_OUTPUT, pidOutput);
    }


    private void updatePidTweakerValues()
    {
        try
        {
            double setPoint = 0;
            final boolean enabledStatus;
            
            if (Robot2014.IN_DEBUG_MODE)
            {
                d = SmartDashboard.getNumber(SDKeys.PID_TWEAKER_D);
                i = SmartDashboard.getNumber(SDKeys.PID_TWEAKER_I);
                p = SmartDashboard.getNumber(SDKeys.PID_TWEAKER_P);
                ff = SmartDashboard.getNumber(SDKeys.PID_TWEAKER_FF);

                setPoint = SmartDashboard.getNumber(SDKeys.PID_TWEAKER_Set);
                setSetpoint(setPoint);
                enabledStatus = updatePIDEnabledStatusFromDashboard();


                getPIDController().setPID(p, i, d, ff);
                final String pidStatus = "Read enabled: " + enabledStatus + " p: " + p + "  i: " + i + "  d: " + d + "  ff: " + ff + "  setSetpoint: " + setPoint + " pidOutput: " + pidOutput;
                SmartDashboard.putString(SDKeys.PID_STATUS_STRING, pidStatus);
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.subsystems.TurretSubsystem.updatePidTweakerValues()", e);
        }
    }


    public void updatePotentiometerDisplay()
    {
        if (isSubsystemEnabled() && potentiometer != null)
        {
            SmartDashboard.putNumber(SDKeys.TURRET_POT_RAW_VALUE, getPotentiometerRawValue());
            SmartDashboard.putString(SDKeys.TURRET_POT_RAW_STRING_VALUE, Double.toString(getPotentiometerRawValue()));

            SmartDashboard.putNumber(SDKeys.TURRET_POT_PID_VALUE, getPotentiometerPidValue());
            SmartDashboard.putString(SDKeys.TURRET_POT_PID_STRING_VALUE, Double.toString(getPotentiometerPidValue()));
        }
    }


    private double getPotentiometerRawValue()
    {
        return Math2.roundToNPlaces(potentiometer.get(), ROUNDING_PLACES);
    }


    private double getPotentiometerPidValue()
    {
        return Math2.roundToNPlaces(potentiometer.pidGet(), ROUNDING_PLACES);
    }


    public double getPotentiometerAdjustedValue()
    {
        return adjustPotAngleToNominalAngle(Math2.roundToNPlaces(potentiometer.get(), ROUNDING_PLACES));
    }

    public void updateMotorSpeedDisplay()
    {
        try
        {
            if (isSubsystemEnabled())
            {
                if (speedController != null)
                {
                    SmartDashboard.putString(SDKeys.TURRET_MOTOR_STATUS, getSpeedPercentage());
                }
                else
                {
                    SmartDashboard.putString(SDKeys.TURRET_MOTOR_STATUS, "isNull");
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in TurretSubsystem.updateMotorSpeedDisplay()", e);
        }

    }

    public String getSpeedPercentage()
    {
        if (speedController.get() == 0)
        {
            return "STOPPED";
        }
        return Math2.toPercentage(speedController.get());
    }


    public void stopAndDisable()
    {
        // Calling stop (i.e. setting speed to 0) and then disable() is redundant since disable sets the speed to 0;
        // but given the critically of stopping the turret, we double up to be absolutely sure.

        //We double set the speed to zero so we can initialize the stop ASAP, but we want to make sure a read of the SmartDashboard's enable value does not then
        //re-enable it. So we do it again. Yes... overkill... but we want to ensure a stop is executed immediately and correctly.
        try { speedController.set(0); }
        catch (Exception e) { LOG.error("An exception occurred when attempting to set the Turret Speed Controller value to 0 to stop it: " + e.toString(), e); }

        try { disable(); }
        catch (Exception e) { LOG.error("An exception occurred when attempting to disable the TurretSubsystem: " + e.toString(), e); }

        try { SmartDashboard.putBoolean(SDKeys.PID_TWEAKER_ENABLED, false); }
        catch (Exception e) { LOG.error("An exception occurred when attempting to update the SmartDashboard PIDTweakerEnable value: " + e.toString(), e); }

        try { speedController.set(0); }
        catch (Exception e) { LOG.error("An exception occurred when attempting to set the Turret Speed Controller value to 0 to stop it: " + e.toString(), e); }

        try { disable(); }
        catch (Exception e) { LOG.error("An exception occurred when attempting to disable the TurretSubsystem: " + e.toString(), e); }
    }


    public boolean isSubsystemEnabled()
    {
        return EM.isTurretSubsystemEnabled;
    }


    AnalogPotentiometer getPotentiometer()
    {
        return potentiometer;
    }

}