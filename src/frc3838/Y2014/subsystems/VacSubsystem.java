package frc3838.Y2014.subsystems;


import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.controls.PWMChannels;
import frc3838.Y2014.subsystems.motors.MotorOps;
import frc3838.Y2014.utils.LOG;



public class VacSubsystem extends Subsystem
{

    private static final VacSubsystem singleton = new VacSubsystem();

    private final static boolean inverseMotor = false; 
     
    private MotorOps motorOps; 
    
    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new VacSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     VacSubsystem subsystem = new VacSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     VacSubsystem subsystem = VacSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static VacSubsystem getInstance() {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     VacSubsystem subsystem = new VacSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     VacSubsystem subsystem = VacSubsystem.getInstance();
     * </pre>
     */
    private VacSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in VacSubsystem() constructor", e);
        }
    }


    private void init()
    {
        if (isEnabled())
        {
            try
            {
                LOG.debug("Initializing VacSubsystem");
                
                motorOps = new MotorOps(new Victor(PWMChannels.VAC_MOTORS), inverseMotor);
                
                LOG.debug("VacSubsystem initialization completed successfully");

            }
            catch (Exception e)
            {
                EM.isVacSubsystemEnabled = false;
                LOG.error("An exception occurred in VacSubsystem.init()", e);
            }
        }
        else
        {
            LOG.info("VacSubsystem is disabled and will not be initialized");
        }
    }


    public MotorOps getMotorOps()
    {
        return motorOps;
    }


    public void initDefaultCommand()
    {
        //setDefaultCommand(new MyCommand());
    }


    public boolean isEnabled()
    {
        return EM.isVacSubsystemEnabled;
    }
}
