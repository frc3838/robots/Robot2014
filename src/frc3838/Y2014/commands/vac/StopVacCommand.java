package frc3838.Y2014.commands.vac;

import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.utils.LOG;



public class StopVacCommand extends CommandBase
{
    private double speed;
    
    private StartVacCommand startVacCommand;
    StopVacCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(vacSubsystem);
    }


    void setStartVacCommand(StartVacCommand startVacCommand)
    {
        this.startVacCommand = startVacCommand;
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        if (allSubsystemsAreEnabled())
        {
            if (startVacCommand.isRunning())
            {
                startVacCommand.cancel();
            }
            speed = vacSubsystem.getMotorOps().getSpeed();
        }
        else
        {
            LOG.info("Not all required subsystems for StopVacCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                //We want to ramp down (per Scott) to prevent any surges.
                //We drop 10 percent on each call, and the isFinished method
                //returns true once we are stopped. However, we do a check here to
                //prevent a timing issue that ends up putting the motor in reverse
                vacSubsystem.getMotorOps().decreaseForwardSpeedFivePercent();
                speed = vacSubsystem.getMotorOps().getSpeed();
                if (Math.abs(speed) <= .1)
                {
                    speed = 0;
                    vacSubsystem.getMotorOps().stop();
                }
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in StopVacCommand.execute()", e);
            }
        }
        else
        {
            LOG.trace("Not all required subsystems for StopVacCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return speed == 0 || !vacSubsystem.getMotorOps().isRunning();
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return EM.isVacSubsystemEnabled;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
        //No Op
    }
    
    
}
