package frc3838.Y2014.commands.flipper;

import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.subsystems.flipper.FlipperSubsystem;
import frc3838.Y2014.utils.LOG;



public class OpenFlipperCommand extends CommandBase
{
    private FlipperSubsystem flipperSubsystem;

    public OpenFlipperCommand(FlipperSubsystem flipperSubsystem)
    {
        requires(rightFlipperSubsystem);
        requires(leftFlipperSubsystem);
        this.flipperSubsystem = flipperSubsystem;
        requires(this.flipperSubsystem);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            // No op
        }
        else
        {
            LOG.info("Not all required subsystems for OpenFlipperCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                LOG.trace("Open flipper called on " + flipperSubsystem.getName());
                //We call & check isFinished so we do not start the motor if the flipper is already in position (i.e. opened)
                if (!isFinished())
                {
                    flipperSubsystem.startMotorOpening();
                }
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in OpenFlipperCommand.execute()", e);
            }
        }
        else
        {
            LOG.trace("Not all required subsystems for OpenFlipperCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return flipperSubsystem.getOpeningLimitSwitch().get();
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (flipperSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
        flipperSubsystem.stopMotor();
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
