package frc3838.Y2014.commands.time;


import frc3838.Y2014.Robot2014;
import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.utils.LOG;



public class SleepSecondsCommand extends CommandBase
{
    private long startTime;
    private long delay;
    private boolean finished = false;


    public SleepSecondsCommand(float seconds)
    {
        delay = (long) (seconds * 1000);
        if (Robot2014.IN_DEBUG_MODE)
        {
            LOG.debug("SLEEP Seconds command created for " + seconds + " = " + delay + "ms");
        }
    }


    public SleepSecondsCommand(int seconds)
    {
        delay = (long) (seconds * 1000);
        if (Robot2014.IN_DEBUG_MODE)
        {
            LOG.debug("SLEEP Seconds command created for " + seconds + " = " + delay + "ms");
        }
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        if (Robot2014.IN_DEBUG_MODE)
        {
            LOG.debug("SLEEP Seconds command starting for " + delay + "ms");
        }

        startTimer();
    }


    private void startTimer()
    {
        finished = false;
        startTime = System.currentTimeMillis();
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        long diff = System.currentTimeMillis() - startTime;
        finished = diff >= delay;
        if (finished)
        {
            LOG.debug("SLEEP Seconds Command Stopping after " + delay + "ms");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return finished;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
