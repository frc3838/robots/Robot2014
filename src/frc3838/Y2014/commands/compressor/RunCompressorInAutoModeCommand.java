package frc3838.Y2014.commands.compressor;

import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.utils.LOG;



public class RunCompressorInAutoModeCommand extends CommandBase
{


    public RunCompressorInAutoModeCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(compressorSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        if (allSubsystemsAreEnabled())
        {
            compressorSubsystem.startCompressorAndAutoRun();
        }
        else
        {
            LOG.info("Not all required subsystems for RunCompressorInAutoModeCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            //noinspection EmptyTryBlock
            try
            {
                //No execution code at this time. We just start the compressor in the initialize method
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in RunCompressorInAutoModeCommand.execute()", e);
            }

        }
        else
        {
            LOG.trace("Not all required subsystems for RunCompressorInAutoModeCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (compressorSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
