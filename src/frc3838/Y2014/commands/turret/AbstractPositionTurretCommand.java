package frc3838.Y2014.commands.turret;

import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.utils.LOG;



public abstract class AbstractPositionTurretCommand extends CommandBase
{


    protected AbstractPositionTurretCommand()
    {
        requires(turretSubsystem);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        if (allSubsystemsAreEnabled())
        {
            //no op
        }
        else
        {
            LOG.info("Not all required subsystems for AbstractPositionTurretCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                turretSubsystem.setPositionTo(getTargetPosition());
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in AbstractPositionTurretCommand.execute()", e);
            }
        }
        else
        {
            LOG.trace("Not all required subsystems for AbstractPositionTurretCommand are enabled. The command can not be and will not be executed.");
        }
    }


    protected abstract double getTargetPosition();


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (turretSubsystem.isSubsystemEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
