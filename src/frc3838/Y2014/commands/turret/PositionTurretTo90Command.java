package frc3838.Y2014.commands.turret;


public class PositionTurretTo90Command extends AbstractPositionTurretCommand
{
    
    
    protected double getTargetPosition()
    {
        return 90.0;
    }
}
