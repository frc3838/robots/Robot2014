package frc3838.Y2014.commands.turret;


public class PositionTurretTo180Command extends AbstractPositionTurretCommand
{
    
    
    protected double getTargetPosition()
    {
        return 180.0;
    }
}
