package frc3838.Y2014.commands.turret;

import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.utils.LOG;



public class DisplayPotValueCommand extends CommandBase
{

    public DisplayPotValueCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(turretSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        if (allSubsystemsAreEnabled())
        {
            turretSubsystem.updatePotentiometerDisplay();
        }
        else
        {
            LOG.info("Not all required subsystems for DisplayPotValueCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                turretSubsystem.updatePotentiometerDisplay();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in DisplayPotValueCommand.execute()", e);
            }

        }
        else
        {
            LOG.trace("Not all required subsystems for DisplayPotValueCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (turretSubsystem.isSubsystemEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
