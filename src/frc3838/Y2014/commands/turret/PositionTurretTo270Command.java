package frc3838.Y2014.commands.turret;


public class PositionTurretTo270Command extends AbstractPositionTurretCommand
{
    
    
    protected double getTargetPosition()
    {
        return 270.0;
    }
}
